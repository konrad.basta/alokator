//
// Created by root on 10/30/20.
//
//
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include "heap.h"
#include "custom_unistd.h"
#include <string.h>

#define HEADER_SIZE sizeof(memory_chunk_t)
#define FENCES_SIZE 16
#define FENCE_SIZE 8
#define CONTROL (HEADER_SIZE+FENCE_SIZE)
#define GUARD (HEADER_SIZE+FENCES_SIZE)
#define NEW_BEHIND (HEADER_SIZE+CONTROL)
#define ALIGNMENT sizeof(void*)
#define ALIGN(x) (((x)+(ALIGNMENT - 1)) & ~(ALIGNMENT - 1))
#define ALIGN_PAGE(x) (((x)+(PAGE_SIZE - 1)) & ~(PAGE_SIZE - 1))

//memory_manager_t memoryManager = {NULL, 0, NULL, NULL};
memory_manager_t memoryManager;

void __attribute__((constructor)) myConstructor(void) {
    memoryManager.start = NULL;
    memoryManager.firstChunk = NULL;
    memoryManager.lastChunk = NULL;
    memoryManager.size = 0;
}

int heap_setup(void) {
    void *ptr = custom_sbrk(0);
    if (ptr == (void *) -1)
        return -1;

    memoryManager.start = ptr;
    memoryManager.size = 0;
    memoryManager.firstChunk = NULL;
    memoryManager.lastChunk = NULL;
    int result = pthread_mutexattr_init(&memoryManager.mutexattr);
    if (result)
        return -1;
    pthread_mutexattr_settype(&memoryManager.mutexattr, PTHREAD_MUTEX_RECURSIVE);
    result = pthread_mutex_init(&memoryManager.mutex,&memoryManager.mutexattr);
    if (result)
        return -1;
    return 0;
}

void heap_clean(void) {
    custom_sbrk((-1) * (int64_t) memoryManager.size);
    memoryManager.start = NULL;
    memoryManager.size = 0;
    memoryManager.firstChunk = NULL;
    memoryManager.lastChunk = NULL;
    pthread_mutex_destroy(&memoryManager.mutex);
}

void *heap_malloc(size_t size) {
    pthread_mutex_lock(&memoryManager.mutex);

    if (heap_validate() || !size) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    size_t count = ALIGN(size + FENCES_SIZE);
    size_t newChunkStart = count + HEADER_SIZE;
    size_t block = size + FENCES_SIZE;
    memory_chunk_t *memoryChunk = memoryManager.firstChunk;

    while (memoryChunk) {
        if (memoryChunk->free && memoryChunk->size >= count) {
            memoryChunk->fileline = __LINE__;
            memoryChunk->filename = __FILE__;
            test(memoryChunk);
            if (memoryChunk->size >= newChunkStart) {
                memory_chunk_t *newMemoryChunk = (memory_chunk_t *) ((uint8_t *) memoryChunk + newChunkStart);
                newMemoryChunk->prev = memoryChunk;
                newMemoryChunk->next = memoryChunk->next;
                newMemoryChunk->size = memoryChunk->size - newChunkStart;
                newMemoryChunk->free = 1;
                newMemoryChunk->unused = 0;
                newMemoryChunk->fileline = __LINE__;
                newMemoryChunk->filename = __FILE__;
                test(newMemoryChunk);
                memoryChunk->next = newMemoryChunk;
                memoryChunk->unused = count - block;
                if (newMemoryChunk->next) {
                    newMemoryChunk->next->prev = newMemoryChunk;
                    newMemoryChunk->next->fileline = __LINE__;
                    newMemoryChunk->next->filename = __FILE__;
                    test(newMemoryChunk->next);
                } else {
                    memoryManager.lastChunk = newMemoryChunk;
                }
            } else
                memoryChunk->unused = memoryChunk->size - block;
            memoryChunk->free = 0;
            memoryChunk->size = size;
            heap_fences_set(memoryChunk);
            pthread_mutex_unlock(&memoryManager.mutex);
            return (uint8_t *) memoryChunk + CONTROL;
        }
        memoryChunk = memoryChunk->next;
    }

    memoryChunk = custom_sbrk(newChunkStart);
    if (memoryChunk == (void *) -1) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }


    if (memoryManager.firstChunk) {
        memoryChunk->prev = memoryManager.lastChunk;
        memoryChunk->prev->fileline = __LINE__;
        memoryChunk->prev->filename = __FILE__;
        test(memoryChunk->prev);
        memoryManager.lastChunk->next = memoryChunk;
    } else {
        memoryChunk->prev = NULL;
        memoryManager.firstChunk = memoryChunk;
    }
    memoryChunk->next = NULL;
    memoryChunk->size = size;
    memoryChunk->free = 0;
    memoryChunk->unused = count - block;
    memoryChunk->fileline = __LINE__;
    memoryChunk->filename = __FILE__;
    test(memoryChunk);
    memoryManager.lastChunk = memoryChunk;
    memoryManager.size += newChunkStart;

    heap_fences_set(memoryChunk);
    pthread_mutex_unlock(&memoryManager.mutex);
    return (uint8_t *) memoryChunk + CONTROL;
}

void *heap_calloc(size_t number, size_t size) {
    pthread_mutex_lock(&memoryManager.mutex);
    uint8_t *memoryChunk = heap_malloc(number * size);
    if (!memoryChunk) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    size_t fill = number * size;
    while (fill--)
        memoryChunk[fill] = 0;

    memory_chunk_t *secMemoryChunk = (memory_chunk_t *)(memoryChunk - FENCE_SIZE - HEADER_SIZE);
    secMemoryChunk->fileline = __LINE__;
    secMemoryChunk->filename = __FILE__;
    test(secMemoryChunk);

    pthread_mutex_unlock(&memoryManager.mutex);
    return memoryChunk;
}

void *heap_realloc(void *memblock, size_t count) {
    pthread_mutex_lock(&memoryManager.mutex);
    if (heap_validate()) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    if (!memblock) {
        void *result = heap_malloc(count);
        pthread_mutex_unlock(&memoryManager.mutex);
        return result;
    }

    if (!count) {
        heap_free(memblock);
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }


    size_t size = ALIGN(count + FENCES_SIZE);
    memory_chunk_t *memoryChunk = memoryManager.firstChunk;
    while (memoryChunk) {
        if ((uint8_t *) memoryChunk + CONTROL == memblock) {
            memoryChunk->fileline = __LINE__;
            memoryChunk->filename = __FILE__;
            test(memoryChunk);
            size_t availableMemory = memoryChunk->size + memoryChunk->unused;
            size_t block = size - FENCES_SIZE;
            if (memoryChunk->size == count) {
                pthread_mutex_unlock(&memoryManager.mutex);
                return memblock;
            }
            if (memoryChunk->size < count) {
                if (memoryChunk->size + memoryChunk->unused >= count) {
                    memoryChunk->unused -= (int) (count - memoryChunk->size);
                    memoryChunk->size = count;
                    heap_fences_set(memoryChunk);
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
                if (memoryChunk->next) {
                    size_t availableMemory2 = availableMemory + HEADER_SIZE + memoryChunk->next->size;
                    if (memoryChunk->next->free && availableMemory2 >= size) {
                        if (availableMemory2 + FENCES_SIZE - size >= HEADER_SIZE) {
                            memory_chunk_t *copyNext = memoryChunk->next->next;
                            memory_chunk_t *newMemoryChunk;
                            newMemoryChunk = (memory_chunk_t *) ((uint8_t *)memoryChunk + HEADER_SIZE + size);
                            newMemoryChunk->prev = memoryChunk;
                            newMemoryChunk->next = copyNext;
                            newMemoryChunk->size = availableMemory2 + FENCES_SIZE - size - HEADER_SIZE;
                            newMemoryChunk->free = 1;
                            newMemoryChunk->unused = 0;
                            newMemoryChunk->fileline = __LINE__;
                            newMemoryChunk->filename = __FILE__;
                            test(newMemoryChunk);

                            memoryChunk->next = newMemoryChunk;
                            memoryChunk->unused = (intptr_t) newMemoryChunk - (intptr_t) memoryChunk - count - GUARD;
                            if (newMemoryChunk->next) {
                                newMemoryChunk->next->prev = newMemoryChunk;
                                newMemoryChunk->next->fileline = __LINE__;
                                newMemoryChunk->next->filename = __FILE__;
                                test(newMemoryChunk->next);
                            } else {
                                memoryManager.lastChunk = newMemoryChunk;
                            }
                        } else {
                            memoryChunk->unused = (int) (availableMemory2 - count);
                            memoryChunk->next = memoryChunk->next->next;
                            if (memoryChunk->next) {
                                memoryChunk->next->prev = memoryChunk;
                                memoryChunk->next->fileline = __LINE__;
                                memoryChunk->next->filename = __FILE__;
                                test(memoryChunk->next);
                            } else {
                                memoryManager.lastChunk = memoryChunk;
                            }
                        }
                        memoryChunk->size = count;
                        heap_fences_set(memoryChunk);
                        pthread_mutex_unlock(&memoryManager.mutex);
                        return memblock;
                    } else {
                        memory_chunk_t *newMemoryChunk = heap_malloc(count);
                        if (!newMemoryChunk) {
                            pthread_mutex_unlock(&memoryManager.mutex);
                            return NULL;
                        }

                        mempcpy((uint8_t *) newMemoryChunk, memblock, availableMemory);
                        heap_free(memblock);
                        pthread_mutex_unlock(&memoryManager.mutex);
                        return (uint8_t *) newMemoryChunk;
                    }
                } else {
                    void *newMemoryChunk = custom_sbrk(block - availableMemory);
                    if (newMemoryChunk == (void *) -1) {
                        pthread_mutex_unlock(&memoryManager.mutex);
                        return NULL;
                    }

                    memoryManager.size += block - availableMemory;
                    memoryChunk->size = count;
                    memoryChunk->unused = block - count;
                    heap_fences_set(memoryChunk);
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
            } else {
                if (availableMemory - block >= HEADER_SIZE) {
                    memory_chunk_t *newMemoryChunk;
                    newMemoryChunk = (memory_chunk_t *) ((uint8_t *) memoryChunk + HEADER_SIZE + size);
                    newMemoryChunk->prev = memoryChunk;
                    newMemoryChunk->next = memoryChunk->next;
                    newMemoryChunk->size = availableMemory - block - HEADER_SIZE;
                    newMemoryChunk->free = 1;
                    newMemoryChunk->unused = 0;
                    newMemoryChunk->fileline = __LINE__;
                    newMemoryChunk->filename = __FILE__;
                    test(newMemoryChunk);

                    memoryChunk->unused = (intptr_t) newMemoryChunk - (intptr_t) memoryChunk - count - GUARD;
                    memoryChunk->next = newMemoryChunk;
                    if (newMemoryChunk->next) {
                        newMemoryChunk->next->prev = newMemoryChunk;
                        newMemoryChunk->next->fileline = __LINE__;
                        newMemoryChunk->next->filename = __FILE__;
                        test(newMemoryChunk->next);
                    } else {
                        memoryManager.lastChunk = newMemoryChunk;
                    }
                } else {
                    memoryChunk->unused = (int) (availableMemory - count);
                }
                memoryChunk->size = count;
                heap_fences_set(memoryChunk);
                pthread_mutex_unlock(&memoryManager.mutex);
                return memblock;
            }
        }
        memoryChunk = memoryChunk->next;
    }
    pthread_mutex_unlock(&memoryManager.mutex);
    return NULL;
}

void heap_free(void *memblock) {
    pthread_mutex_lock(&memoryManager.mutex);
    if (heap_validate()){
        pthread_mutex_unlock(&memoryManager.mutex);
        return;
    }
    memory_chunk_t *memoryChunk = memoryManager.firstChunk;
    uint8_t *memorySearched = memblock;
    memorySearched -= CONTROL;
    while (memoryChunk) {
        if (memoryChunk == (memory_chunk_t *) memorySearched) {
            memoryChunk->free = 1;
            memoryChunk->size += memoryChunk->unused + FENCES_SIZE;
            memoryChunk->unused = 0;
            if (memoryChunk->prev && memoryChunk->prev->free) {
                memoryChunk->prev->fileline = __LINE__;
                memoryChunk->prev->filename = __FILE__;
                test(memoryChunk->prev);
                memoryChunk->prev->next = memoryChunk->next;
                memoryChunk->prev->size += memoryChunk->size + HEADER_SIZE;
                if (memoryChunk->next) {
                    memoryChunk->next->prev = memoryChunk->prev;
                    memoryChunk->next->fileline = __LINE__;
                    memoryChunk->next->filename = __FILE__;
                    test(memoryChunk->next);
                } else {
                    memoryManager.lastChunk = memoryChunk->prev;
                }
                memoryChunk = memoryChunk->prev;
            } else {
                memoryChunk->fileline = __LINE__;
                memoryChunk->filename = __FILE__;
                test(memoryChunk);
            }
            if (memoryChunk->next && memoryChunk->next->free) {
                if (memoryChunk->next == memoryManager.lastChunk) {
                    memoryManager.lastChunk = memoryChunk;
                }
                if (memoryChunk->next->next) {
                    memoryChunk->next->next->prev = memoryChunk;
                    memoryChunk->next->next->fileline = __LINE__;
                    memoryChunk->next->next->filename = __FILE__;
                    test(memoryChunk->next->next);
                }
                memoryChunk->size += memoryChunk->next->size + HEADER_SIZE;
                memoryChunk->next = memoryChunk->next->next;
            }
            pthread_mutex_unlock(&memoryManager.mutex);
            return;
        }
        memoryChunk = memoryChunk->next;
    }
    pthread_mutex_unlock(&memoryManager.mutex);
}

size_t heap_get_largest_used_block_size(void) {
    pthread_mutex_lock(&memoryManager.mutex);
    if (heap_validate()) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return 0;
    }

    size_t maxSize = 0;
    memory_chunk_t *memoryChunk = memoryManager.firstChunk;
    while (memoryChunk) {
        if (!memoryChunk->free && memoryChunk->size > maxSize)
            maxSize = memoryChunk->size;
        memoryChunk = memoryChunk->next;
    }
    pthread_mutex_unlock(&memoryManager.mutex);
    return maxSize;
}

enum pointer_type_t get_pointer_type(const void *const pointer) {
    pthread_mutex_lock(&memoryManager.mutex);
    if (!pointer) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return pointer_null;
    }

    if (heap_validate()) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return pointer_heap_corrupted;
    }

    memory_chunk_t *memoryChunk = memoryManager.firstChunk;
    uint8_t *memoryBlock = (uint8_t *) memoryChunk + CONTROL;
    uint8_t *ptr = (uint8_t *) pointer;

    if (pointer < memoryManager.start || ptr > ((uint8_t *) memoryManager.lastChunk + HEADER_SIZE + memoryManager.lastChunk->size + memoryManager.lastChunk->unused + (memoryManager.lastChunk->free ? 0 : FENCES_SIZE))) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return pointer_unallocated;
    }

    while (memoryChunk) {
        if (ptr >= (uint8_t *) memoryChunk && ptr < ((uint8_t *) memoryChunk + HEADER_SIZE)) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return pointer_control_block;
        }
        if (ptr < memoryBlock) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return memoryChunk->free ? pointer_unallocated : pointer_inside_fences;
        }
        if (ptr == memoryBlock) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return memoryChunk->free ? pointer_unallocated : pointer_valid;
        }
        if (ptr < memoryBlock + memoryChunk->size) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return memoryChunk->free ? pointer_unallocated : pointer_inside_data_block;
        }
        if (ptr < memoryBlock + memoryChunk->size + FENCE_SIZE) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return memoryChunk->free ? pointer_unallocated : pointer_inside_fences;
        }
        if (ptr < memoryBlock + memoryChunk->size + FENCE_SIZE + memoryChunk->unused) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return pointer_unallocated;
        }
        memoryChunk = memoryChunk->next;
        memoryBlock = (uint8_t *) memoryChunk + CONTROL;
    }

    pthread_mutex_unlock(&memoryManager.mutex);
    return pointer_heap_corrupted;
}

int heap_validate(void) {
    pthread_mutex_lock(&memoryManager.mutex);
    if (!memoryManager.start) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return 2;
    }

    if (memoryManager.firstChunk) {
        if (memoryManager.lastChunk) {
            if ((memoryManager.firstChunk != memoryManager.start) || (memoryManager.size <= HEADER_SIZE)
                || ((uint8_t *) memoryManager.lastChunk >
                    (uint8_t *) memoryManager.start + memoryManager.size - HEADER_SIZE)
                || memoryManager.lastChunk->next || memoryManager.firstChunk->prev) {
                pthread_mutex_unlock(&memoryManager.mutex);
                return 3;
            }
        } else {
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }
    } else if (memoryManager.lastChunk) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return 3;
    } else if (memoryManager.size > 0) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return 3;
    } else {
        pthread_mutex_unlock(&memoryManager.mutex);
        return 0;
    }

    memory_chunk_t *memoryChunk = memoryManager.firstChunk;
    memory_chunk_t *prev = NULL;
    size_t memorySize = 0;
    while (memoryChunk) {
        if (memoryChunk < memoryManager.firstChunk || memoryChunk > memoryManager.lastChunk) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }
        if (!memoryChunk->prev && memoryChunk != memoryManager.firstChunk) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }
        if (!memoryChunk->next && memoryChunk != memoryManager.lastChunk) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }
        if (memoryChunk->prev != prev) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }
        prev = memoryChunk;
        if (memoryChunk->next &&
            (memoryChunk->next < memoryManager.firstChunk || memoryChunk->next > memoryManager.lastChunk ||
             memoryChunk->next < memoryChunk)) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }
        if (memoryChunk->next && (uint8_t *) memoryChunk->next !=
                                 (uint8_t *) memoryChunk + memoryChunk->size + memoryChunk->unused + HEADER_SIZE +
                                 (memoryChunk->free ? 0 : FENCES_SIZE)) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }
        if (memoryChunk->free != 0 && memoryChunk->free != 1) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }
        if (memoryChunk->free && memoryChunk->unused) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }
        if (memoryChunk->unused < 0 || (size_t) memoryChunk->unused > memoryManager.size) {  //Do poprawy
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }

        memorySize += HEADER_SIZE + memoryChunk->size + memoryChunk->unused + (memoryChunk->free ? 0 : FENCES_SIZE);
        if (memorySize > memoryManager.size) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }
        if (!memoryChunk->next && memorySize < memoryManager.size) {
            uint64_t reserved_memory = custom_sbrk_get_reserved_memory();
            (void) reserved_memory;
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }


        if (!memoryChunk->free) {
            uint8_t *memoryfance = (uint8_t *) memoryChunk;
            memoryfance += HEADER_SIZE;
            for (int i = 0; i < FENCE_SIZE; ++i)
                if (memoryfance[i] != memoryfance[i + FENCE_SIZE + memoryChunk->size]) {
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return 1;
                }
        }

        if (memoryChunk->test != memoryChunk->fileline + (intptr_t)memoryChunk->filename) {
            pthread_mutex_unlock(&memoryManager.mutex);
            return 3;
        }

        memoryChunk = memoryChunk->next;
    }

    pthread_mutex_unlock(&memoryManager.mutex);
    return 0;
}

void *heap_malloc_aligned(size_t count) {
    pthread_mutex_lock(&memoryManager.mutex);
    if (heap_validate() || !count) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    size_t block = GUARD + count;
    size_t size = ALIGN(count + FENCES_SIZE);
    size_t move;
    memory_chunk_t *memoryChunk = memoryManager.firstChunk;
    while (memoryChunk) {
        move = PAGE_SIZE - ((intptr_t) memoryChunk % PAGE_SIZE);
        if (memoryChunk->free) {
            if (move < CONTROL) {
                move += PAGE_SIZE;
            }
            if (move + size < memoryChunk->size) {
                memory_chunk_t * copyPrev = memoryChunk->prev;
                memory_chunk_t * copyNext = memoryChunk->next;
                size_t copySize = memoryChunk->size;

                memory_chunk_t *newMemoryChunk = (memory_chunk_t *) ((intptr_t) memoryChunk + move - CONTROL);
                newMemoryChunk->size = count;
                newMemoryChunk->free = 0;
                newMemoryChunk->next = copyNext;
                newMemoryChunk->fileline = __LINE__;
                newMemoryChunk->filename = __FILE__;
                test(newMemoryChunk);

                if (move >= NEW_BEHIND) {
                    memoryChunk->next = newMemoryChunk;
                    memoryChunk->size = move - NEW_BEHIND;
                    memoryChunk->fileline = __LINE__;
                    memoryChunk->filename = __FILE__;
                    test(memoryChunk);
                    newMemoryChunk->prev = memoryChunk;
                } else {
                    copyPrev->next = newMemoryChunk;
                    copyPrev->unused += move - CONTROL;
                    newMemoryChunk->prev = copyPrev;
                }

                memory_chunk_t *secondNewChunk = NULL;
                if (copySize >= size + FENCE_SIZE) {
                    secondNewChunk = (memory_chunk_t *) ((intptr_t) newMemoryChunk + HEADER_SIZE + size);
                    secondNewChunk->prev = newMemoryChunk;
                    secondNewChunk->next = newMemoryChunk->next;
                    secondNewChunk->size = copySize + FENCE_SIZE - move - size;
                    secondNewChunk->free = 1;
                    secondNewChunk->unused = 0;
                    secondNewChunk->fileline = __LINE__;
                    secondNewChunk->filename = __FILE__;
                    test(secondNewChunk);

                    newMemoryChunk->next = secondNewChunk;
                    newMemoryChunk->unused = (intptr_t) secondNewChunk - (intptr_t) newMemoryChunk - block;

                    if (secondNewChunk->next) {
                        secondNewChunk->next->prev = secondNewChunk;
                        secondNewChunk->next->fileline = __LINE__;
                        secondNewChunk->next->filename = __FILE__;
                        test(secondNewChunk->next);
                    } else {
                        memoryManager.lastChunk = secondNewChunk;
                    }
                } else {
                    newMemoryChunk->unused = copySize + HEADER_SIZE - move - count - FENCE_SIZE;
                    if (newMemoryChunk->next) {
                        newMemoryChunk->next->prev = newMemoryChunk;
                        newMemoryChunk->next->fileline = __LINE__;
                        newMemoryChunk->next->filename = __FILE__;
                        test(newMemoryChunk->next);
                    } else {
                        memoryManager.lastChunk->next = newMemoryChunk;
                        memoryManager.lastChunk->fileline = __LINE__;
                        memoryManager.lastChunk->filename = __FILE__;
                        test(memoryManager.lastChunk);
                        memoryManager.lastChunk = newMemoryChunk;
                    }
                }
                heap_fences_set(newMemoryChunk);
                pthread_mutex_unlock(&memoryManager.mutex);
                return (uint8_t *) newMemoryChunk + CONTROL;
            }
        }
        memoryChunk = memoryChunk->next;
    }

    void *mem0 = custom_sbrk(0);
    if (memoryManager.lastChunk && memoryManager.lastChunk->free) {
        move = PAGE_SIZE - ((intptr_t) memoryManager.lastChunk % PAGE_SIZE);
        if (move < CONTROL) {
            move += PAGE_SIZE;
        }
        size = ALIGN(move + count + FENCE_SIZE - HEADER_SIZE - memoryManager.lastChunk->size);
    } else {
        move = PAGE_SIZE - ((intptr_t) mem0 % PAGE_SIZE);
        if (move < CONTROL) {
            move += PAGE_SIZE;
        }
        size = ALIGN(move + count + FENCE_SIZE);
    }

    if (custom_sbrk(size) == (void *) -1) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }
    memoryManager.size += size;

    memory_chunk_t *newMemoryChunk = NULL;
    if (!memoryManager.lastChunk || !memoryManager.lastChunk->free ||
        move >= NEW_BEHIND + memoryManager.lastChunk->size) {
        memoryChunk = (memory_chunk_t *) (
                (memoryManager.lastChunk && memoryManager.lastChunk->free ? (uint8_t *) memoryManager.lastChunk
                                                                          : (uint8_t *) mem0) + move - CONTROL);
        memoryChunk->next = NULL;
        memoryChunk->size = count;
        memoryChunk->free = 0;
        memoryChunk->unused = (intptr_t) custom_sbrk(0) - (intptr_t) memoryChunk - block;
        memoryChunk->fileline = __LINE__;
        memoryChunk->filename = __FILE__;
        test(memoryChunk);

        if (memoryManager.lastChunk) {
            if (memoryManager.lastChunk->free) {
                memoryManager.lastChunk->size =
                        (intptr_t) memoryChunk - (intptr_t) memoryManager.lastChunk - HEADER_SIZE;
            } else if (move >= NEW_BEHIND) {
                newMemoryChunk = mem0;
                newMemoryChunk->prev = memoryManager.lastChunk;
                newMemoryChunk->next = memoryChunk;
                newMemoryChunk->size = (intptr_t) memoryChunk - (intptr_t) newMemoryChunk - HEADER_SIZE;
                newMemoryChunk->free = 1;
                newMemoryChunk->unused = 0;

                memoryManager.lastChunk->next = newMemoryChunk;
                memoryManager.lastChunk->fileline = __LINE__;
                memoryManager.lastChunk->filename = __FILE__;
                test(memoryManager.lastChunk);
                memoryManager.lastChunk = newMemoryChunk;
            } else {
                memoryManager.lastChunk->unused = (intptr_t) memoryChunk - (intptr_t) memoryManager.lastChunk - GUARD -
                                                  memoryManager.lastChunk->size;
            }
            memoryChunk->prev = memoryManager.lastChunk;
            memoryManager.lastChunk->next = memoryChunk;
            memoryManager.lastChunk->fileline = __LINE__;
            memoryManager.lastChunk->filename = __FILE__;
            test(memoryManager.lastChunk);
            memoryManager.lastChunk = memoryChunk;
        } else {
            newMemoryChunk = mem0;
            newMemoryChunk->prev = NULL;
            newMemoryChunk->next = memoryChunk;
            newMemoryChunk->size = (intptr_t) memoryChunk - (intptr_t) newMemoryChunk - HEADER_SIZE;
            newMemoryChunk->free = 1;
            newMemoryChunk->unused = 0;
            newMemoryChunk->fileline = __LINE__;
            newMemoryChunk->filename = __FILE__;
            test(newMemoryChunk);
            memoryManager.firstChunk = newMemoryChunk;

            memoryManager.lastChunk = memoryChunk;
            memoryChunk->prev = newMemoryChunk;
        }
    } else {
        memoryChunk = (memory_chunk_t *) ((uint8_t *) memoryManager.lastChunk + move - CONTROL);
        if (move >= NEW_BEHIND) {
            memoryManager.lastChunk->size = (intptr_t) memoryChunk - (intptr_t) memoryManager.lastChunk - HEADER_SIZE;
            memoryManager.lastChunk->next = memoryChunk;
            memoryManager.lastChunk->fileline = __LINE__;
            memoryManager.lastChunk->filename = __FILE__;
            test(memoryManager.lastChunk);
            memoryChunk->prev = memoryManager.lastChunk;
            memoryManager.lastChunk = memoryChunk;
        } else {
            memoryManager.lastChunk->prev->unused += (intptr_t) memoryChunk - (intptr_t) memoryManager.lastChunk;
            memoryManager.lastChunk->prev->next = memoryChunk;
            memoryManager.lastChunk->prev->fileline = __LINE__;
            memoryManager.lastChunk->prev->filename = __FILE__;
            test(memoryManager.lastChunk->prev);
            memoryChunk->prev = memoryManager.lastChunk->prev;
            memoryManager.lastChunk = memoryChunk;
        }
        memoryChunk->next = NULL;
        memoryChunk->size = count;
        memoryChunk->free = 0;
        memoryChunk->unused = (intptr_t) custom_sbrk(0) - (intptr_t) memoryChunk - block;
    }
    heap_fences_set(memoryChunk);
    pthread_mutex_unlock(&memoryManager.mutex);
    return (uint8_t *) memoryChunk + CONTROL;
}

void *heap_calloc_aligned(size_t number, size_t size) {
    pthread_mutex_lock(&memoryManager.mutex);
    uint8_t *memoryChunk = heap_malloc_aligned(number * size);
    if (!memoryChunk) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    size_t fill = number * size;
    while (fill--)
        memoryChunk[fill] = 0;

    memory_chunk_t *secMemoryChunk = (memory_chunk_t *)(memoryChunk - FENCE_SIZE - HEADER_SIZE);
    secMemoryChunk->fileline = __LINE__;
    secMemoryChunk->filename = __FILE__;
    test(secMemoryChunk);
    pthread_mutex_unlock(&memoryManager.mutex);
    return memoryChunk;
}

void *heap_realloc_aligned(void *memblock, size_t size) {
    pthread_mutex_lock(&memoryManager.mutex);
    if (heap_validate()) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    if (!memblock) {
        void *result = heap_malloc_aligned(size);
        pthread_mutex_unlock(&memoryManager.mutex);
        return result;
    }

    if (!size) {
        heap_free(memblock);
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    memory_chunk_t *memoryChunk = memoryManager.firstChunk;
    while (memoryChunk) {
        if ((uint8_t *) memoryChunk + CONTROL == (uint8_t *) memblock) {
            memoryChunk->fileline = __LINE__;
            memoryChunk->filename = __FILE__;
            test(memoryChunk);
            if (((intptr_t) memblock & (intptr_t) (PAGE_SIZE - 1)) == 0) {
                if (memoryChunk->size == size) {
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
                if (memoryChunk->size > size) {
                    memoryChunk->unused = memoryChunk->size + memoryChunk->unused - size;
                    memoryChunk->size = size;
                    heap_fences_set(memoryChunk);
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
                if (memoryChunk->size + memoryChunk->unused >= size) {
                    memoryChunk->unused = memoryChunk->size + memoryChunk->unused - size;
                    memoryChunk->size = size;
                    heap_fences_set(memoryChunk);
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
                if (memoryChunk->next && memoryChunk->next->free &&
                    memoryChunk->size + memoryChunk->unused + HEADER_SIZE + memoryChunk->next->size >= size) {
                    memoryChunk->size += memoryChunk->unused;
                    memoryChunk->unused = 0;
                    if (memoryChunk->size + HEADER_SIZE + memoryChunk->next->size - size >= HEADER_SIZE) {
                        void *cnext = memoryChunk->next->next;
                        size_t csize = memoryChunk->next->size;
                        memory_chunk_t *newMemoryChunk = (memory_chunk_t *) ((intptr_t) memoryChunk + GUARD + size);
                        newMemoryChunk->size = memoryChunk->size + csize - size;
                        newMemoryChunk->prev = memoryChunk;
                        newMemoryChunk->next = cnext;
                        newMemoryChunk->free = 1;
                        newMemoryChunk->unused = 0;
                        newMemoryChunk->fileline = __LINE__;
                        newMemoryChunk->filename = __FILE__;
                        test(newMemoryChunk);
                        if (newMemoryChunk->next) {
                            newMemoryChunk->next->prev = newMemoryChunk;
                            newMemoryChunk->next->fileline = __LINE__;
                            newMemoryChunk->next->filename = __FILE__;
                            test(newMemoryChunk->next);
                        } else {
                            memoryManager.lastChunk = newMemoryChunk;
                        }
                        memoryChunk->next = newMemoryChunk;
                    } else {
                        size_t csize = memoryChunk->next->size;
                        memoryChunk->next = memoryChunk->next->next;
                        if (!memoryChunk->next) {
                            memoryManager.lastChunk = memoryChunk;
                        }
                        memoryChunk->unused += memoryChunk->size + HEADER_SIZE + csize - size;
                    }
                    memoryChunk->size = size;
                    heap_fences_set(memoryChunk);
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
            }
            if (memoryChunk == memoryManager.lastChunk) {
                void *newMemoryChunk = custom_sbrk(size - memoryChunk->size);
                if (newMemoryChunk == (void *) -1) {
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return NULL;
                }
                memoryManager.size += size - memoryChunk->size;
                memoryChunk->size = size;
                heap_fences_set(memoryChunk);
                pthread_mutex_unlock(&memoryManager.mutex);
                return memblock;
            }

            void *newMemoryChunk = heap_malloc_aligned(size);
            if (!newMemoryChunk) {
                pthread_mutex_unlock(&memoryManager.mutex);
                return NULL;
            }

            mempcpy(newMemoryChunk, memblock, memoryChunk->size);
            heap_free(memblock);
            pthread_mutex_unlock(&memoryManager.mutex);
            return (uint8_t *) newMemoryChunk;
        }
        memoryChunk = memoryChunk->next;
    }
    pthread_mutex_unlock(&memoryManager.mutex);
    return NULL;
}

void heap_fences_set(void *memblock) {
    uint8_t *memoryChunk = memblock;
    memory_chunk_t *memoryChunk2 = memblock;
    if (memoryChunk2->free)
        return;
    memoryChunk += HEADER_SIZE;
    srand(time(NULL));

    for (int i = 0; i < FENCE_SIZE; ++i) {
        uint8_t fence = rand() % 0x100;
        memoryChunk[i] = fence;
        memoryChunk[i + FENCE_SIZE + memoryChunk2->size] = fence;
    }
}

void *heap_malloc_debug(size_t size, int fileline, const char *filename) {
    pthread_mutex_lock(&memoryManager.mutex);
    if (heap_validate() || !size) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    size_t count = ALIGN(size + FENCES_SIZE);
    size_t newChunkStart = count + HEADER_SIZE;
    size_t block = size + FENCES_SIZE;
    memory_chunk_t *memoryChunk = memoryManager.firstChunk;

    while (memoryChunk) {
        if (memoryChunk->free && memoryChunk->size >= count) {
            memoryChunk->fileline = fileline;
            memoryChunk->filename = (char *)filename;
            test(memoryChunk);
            if (memoryChunk->size >= newChunkStart) {
                memory_chunk_t *newMemoryChunk = (memory_chunk_t *) ((uint8_t *) memoryChunk + newChunkStart);
                newMemoryChunk->prev = memoryChunk;
                newMemoryChunk->next = memoryChunk->next;
                newMemoryChunk->size = memoryChunk->size - newChunkStart;
                newMemoryChunk->free = 1;
                newMemoryChunk->unused = 0;
                newMemoryChunk->fileline = fileline;
                newMemoryChunk->filename = (char *)filename;
                test(newMemoryChunk);
                memoryChunk->next = newMemoryChunk;
                memoryChunk->unused = count - block;
                if (newMemoryChunk->next) {
                    newMemoryChunk->next->prev = newMemoryChunk;
                    newMemoryChunk->next->fileline = fileline;
                    newMemoryChunk->next->filename = (char *)filename;
                    test(newMemoryChunk->next);
                } else {
                    memoryManager.lastChunk = newMemoryChunk;
                }
            } else
                memoryChunk->unused = memoryChunk->size - block;
            memoryChunk->free = 0;
            memoryChunk->size = size;
            heap_fences_set(memoryChunk);
            pthread_mutex_unlock(&memoryManager.mutex);
            return (uint8_t *) memoryChunk + CONTROL;
        }
        memoryChunk = memoryChunk->next;
    }

    memoryChunk = custom_sbrk(newChunkStart);
    if (memoryChunk == (void *) -1) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    if (memoryManager.firstChunk) {
        memoryChunk->prev = memoryManager.lastChunk;
        memoryChunk->prev->fileline = fileline;
        memoryChunk->prev->filename = (char *)filename;
        test(memoryChunk->prev);
        memoryManager.lastChunk->next = memoryChunk;
    } else {
        memoryChunk->prev = NULL;
        memoryManager.firstChunk = memoryChunk;
    }
    memoryChunk->next = NULL;
    memoryChunk->size = size;
    memoryChunk->free = 0;
    memoryChunk->unused = count - block;
    memoryChunk->fileline = fileline;
    memoryChunk->filename = (char *)filename;
    test(memoryChunk);
    memoryManager.lastChunk = memoryChunk;
    memoryManager.size += newChunkStart;

    heap_fences_set(memoryChunk);
    pthread_mutex_unlock(&memoryManager.mutex);
    return (uint8_t *) memoryChunk + CONTROL;
}

void *heap_calloc_debug(size_t number, size_t size, int fileline, const char *filename) {
    pthread_mutex_lock(&memoryManager.mutex);
    uint8_t *memoryChunk = heap_malloc_debug(number * size, fileline, filename);
    if (!memoryChunk) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    size_t fill = number * size;
    while (fill--)
        memoryChunk[fill] = 0;

    memory_chunk_t *secMemoryChunk = (memory_chunk_t *)(memoryChunk - FENCE_SIZE - HEADER_SIZE);
    secMemoryChunk->fileline = fileline;
    secMemoryChunk->filename = (char *)filename;
    test(secMemoryChunk);
    pthread_mutex_unlock(&memoryManager.mutex);
    return memoryChunk;
}

void *heap_realloc_debug(void *memblock, size_t count, int fileline, const char *filename) {
    pthread_mutex_lock(&memoryManager.mutex);
    if (heap_validate()) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    if (!memblock) {
        void *result = heap_malloc_debug(count,fileline,filename);
        pthread_mutex_unlock(&memoryManager.mutex);
        return result;
    }

    if (!count) {
        heap_free(memblock);
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    size_t size = ALIGN(count + FENCES_SIZE);
    memory_chunk_t *memoryChunk = memoryManager.firstChunk;
    while (memoryChunk) {
        if ((uint8_t *) memoryChunk + CONTROL == memblock) {
            memoryChunk->fileline = fileline;
            memoryChunk->filename = (char *)filename;
            test(memoryChunk);
            size_t availableMemory = memoryChunk->size + memoryChunk->unused;
            size_t block = size - FENCES_SIZE;
            if (memoryChunk->size == count) {
                pthread_mutex_unlock(&memoryManager.mutex);
                return memblock;
            }
            if (memoryChunk->size < count) {
                if (memoryChunk->size + memoryChunk->unused >= count) {
                    memoryChunk->unused -= (int) (count - memoryChunk->size);
                    memoryChunk->size = count;
                    heap_fences_set(memoryChunk);
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
                if (memoryChunk->next) {
                    size_t availableMemory2 = availableMemory + HEADER_SIZE + memoryChunk->next->size;
                    if (memoryChunk->next->free && availableMemory2 >= size) {
                        if (availableMemory2 - size >= HEADER_SIZE) {
                            memory_chunk_t *copyNext = memoryChunk->next->next;
                            memory_chunk_t *newMemoryChunk;
                            newMemoryChunk = (memory_chunk_t *) ((uint8_t *) memoryChunk + HEADER_SIZE + size);
                            newMemoryChunk->prev = memoryChunk;
                            newMemoryChunk->next = copyNext;
                            newMemoryChunk->size = availableMemory2 + FENCES_SIZE - size - HEADER_SIZE;
                            newMemoryChunk->free = 1;
                            newMemoryChunk->unused = 0;
                            newMemoryChunk->fileline = fileline;
                            newMemoryChunk->filename = (char *)filename;
                            test(newMemoryChunk);

                            memoryChunk->next = newMemoryChunk;
                            memoryChunk->unused = (intptr_t) newMemoryChunk - (intptr_t) memoryChunk - count - GUARD;
                            if (newMemoryChunk->next) {
                                newMemoryChunk->next->prev = newMemoryChunk;
                                newMemoryChunk->next->fileline = fileline;
                                newMemoryChunk->next->filename = (char *)filename;
                                test(newMemoryChunk->next);
                            } else {
                                memoryManager.lastChunk = newMemoryChunk;
                            }
                        } else {
                            memoryChunk->unused = (int) (availableMemory2 - count);
                            memoryChunk->next = memoryChunk->next->next;
                            if (memoryChunk->next) {
                                memoryChunk->next->prev = memoryChunk;
                                memoryChunk->next->fileline = fileline;
                                memoryChunk->next->filename = (char *)filename;
                                test(memoryChunk->next);
                            } else {
                                memoryManager.lastChunk = memoryChunk;
                            }
                        }
                        memoryChunk->size = count;
                        heap_fences_set(memoryChunk);
                        pthread_mutex_unlock(&memoryManager.mutex);
                        return memblock;
                    } else {
                        memory_chunk_t *newMemoryChunk = heap_malloc(count);
                        if (!newMemoryChunk) {
                            pthread_mutex_unlock(&memoryManager.mutex);
                            return NULL;
                        }

                        mempcpy((uint8_t *) newMemoryChunk, memblock, availableMemory);
                        heap_free(memblock);
                        pthread_mutex_unlock(&memoryManager.mutex);
                        return (uint8_t *) newMemoryChunk;
                    }
                } else {
                    void *newMemoryChunk = custom_sbrk(block - availableMemory);
                    if (newMemoryChunk == (void *) -1) {
                        pthread_mutex_unlock(&memoryManager.mutex);
                        return NULL;
                    }

                    memoryManager.size += block - availableMemory;
                    memoryChunk->size = count;
                    memoryChunk->unused = block - count;
                    heap_fences_set(memoryChunk);
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
            } else {
                if (availableMemory - block >= HEADER_SIZE) {
                    memory_chunk_t *newMemoryChunk;
                    newMemoryChunk = (memory_chunk_t *) ((uint8_t *) memoryChunk + HEADER_SIZE + size);
                    newMemoryChunk->prev = memoryChunk;
                    newMemoryChunk->next = memoryChunk->next;
                    newMemoryChunk->size = availableMemory - block - HEADER_SIZE;
                    newMemoryChunk->free = 1;
                    newMemoryChunk->unused = 0;
                    newMemoryChunk->fileline = fileline;
                    newMemoryChunk->filename = (char *)filename;
                    test(newMemoryChunk);

                    memoryChunk->unused = (intptr_t) newMemoryChunk - (intptr_t) memoryChunk - count - GUARD;
                    memoryChunk->next = newMemoryChunk;
                    if (newMemoryChunk->next) {
                        newMemoryChunk->next->prev = newMemoryChunk;
                        newMemoryChunk->next->fileline = fileline;
                        newMemoryChunk->next->filename = (char *)filename;
                        test(newMemoryChunk->next);
                    } else {
                        memoryManager.lastChunk = newMemoryChunk;
                    }
                } else {
                    memoryChunk->unused = (int) (availableMemory - count);
                }
                memoryChunk->size = count;
                heap_fences_set(memoryChunk);
                pthread_mutex_unlock(&memoryManager.mutex);
                return memblock;
            }
        }
        memoryChunk = memoryChunk->next;
    }
    pthread_mutex_unlock(&memoryManager.mutex);
    return NULL;
}

void *heap_malloc_aligned_debug(size_t count, int fileline, const char *filename) {
    pthread_mutex_lock(&memoryManager.mutex);
    if (heap_validate() || !count) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    size_t block = GUARD + count;
    size_t size = ALIGN(count + FENCES_SIZE);
    size_t move;
    memory_chunk_t *memoryChunk = memoryManager.firstChunk;
    while (memoryChunk) {
        move = PAGE_SIZE - ((intptr_t) memoryChunk % PAGE_SIZE);
        if (memoryChunk->free) {
            if (move < CONTROL) {
                move += PAGE_SIZE;
            }
            if (move + size < memoryChunk->size) {
                memory_chunk_t * copyPrev = memoryChunk->prev;
                memory_chunk_t * copyNext = memoryChunk->next;
                size_t copySize = memoryChunk->size;

                memory_chunk_t *newMemoryChunk = (memory_chunk_t *) ((intptr_t) memoryChunk + move - CONTROL);
                newMemoryChunk->size = count;
                newMemoryChunk->free = 0;
                newMemoryChunk->next = copyNext;
                newMemoryChunk->fileline = fileline;
                newMemoryChunk->filename = (char *)filename;
                test(newMemoryChunk);

                if (move >= NEW_BEHIND) {
                    memoryChunk->next = newMemoryChunk;
                    memoryChunk->size = move - NEW_BEHIND;
                    memoryChunk->fileline = fileline;
                    memoryChunk->filename = (char *)filename;
                    test(memoryChunk);
                    newMemoryChunk->prev = memoryChunk;
                } else {
                    copyPrev->next = newMemoryChunk;
                    copyPrev->unused += move - CONTROL;
                    newMemoryChunk->prev = copyPrev;
                }

                memory_chunk_t *secondNewChunk = NULL;
                if (copySize >= size + FENCE_SIZE) {
                    secondNewChunk = (memory_chunk_t *) ((intptr_t) newMemoryChunk + HEADER_SIZE + size);
                    secondNewChunk->prev = newMemoryChunk;
                    secondNewChunk->next = newMemoryChunk->next;
                    secondNewChunk->size = copySize + FENCE_SIZE - move - size;
                    secondNewChunk->free = 1;
                    secondNewChunk->unused = 0;
                    secondNewChunk->fileline = fileline;
                    secondNewChunk->filename = (char *)filename;
                    test(secondNewChunk);

                    newMemoryChunk->next = secondNewChunk;
                    newMemoryChunk->unused = (intptr_t) secondNewChunk - (intptr_t) newMemoryChunk - block;

                    if (secondNewChunk->next) {
                        secondNewChunk->next->prev = secondNewChunk;
                        secondNewChunk->next->fileline = fileline;
                        secondNewChunk->next->filename = (char *)filename;
                        test(secondNewChunk->next);
                    } else {
                        memoryManager.lastChunk = secondNewChunk;
                    }
                } else {
                    newMemoryChunk->unused = copySize + HEADER_SIZE - move - count - FENCE_SIZE;
                    if (newMemoryChunk->next) {
                        newMemoryChunk->next->prev = newMemoryChunk;
                        newMemoryChunk->next->fileline = fileline;
                        newMemoryChunk->next->filename = (char *)filename;
                        test(newMemoryChunk->next);
                    } else {
                        memoryManager.lastChunk->next = newMemoryChunk;
                        memoryManager.lastChunk->fileline = fileline;
                        memoryManager.lastChunk->filename = (char *)filename;
                        test(memoryManager.lastChunk);
                        memoryManager.lastChunk = newMemoryChunk;
                    }
                }
                heap_fences_set(newMemoryChunk);
                pthread_mutex_unlock(&memoryManager.mutex);
                return (uint8_t *) newMemoryChunk + CONTROL;
            }
        }
        memoryChunk = memoryChunk->next;
    }

    void *mem0 = custom_sbrk(0);
    if (memoryManager.lastChunk && memoryManager.lastChunk->free) {
        move = PAGE_SIZE - ((intptr_t) memoryManager.lastChunk % PAGE_SIZE);
        if (move < CONTROL) {
            move += PAGE_SIZE;
        }
        size = ALIGN(move + count + FENCE_SIZE - HEADER_SIZE - memoryManager.lastChunk->size);
    } else {
        move = PAGE_SIZE - ((intptr_t) mem0 % PAGE_SIZE);
        if (move < CONTROL) {
            move += PAGE_SIZE;
        }
        size = ALIGN(move + count + FENCE_SIZE);
    }

    if (custom_sbrk(size) == (void *) -1) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }
    memoryManager.size += size;

    memory_chunk_t *newMemoryChunk = NULL;
    if (!memoryManager.lastChunk || !memoryManager.lastChunk->free ||
        move >= NEW_BEHIND + memoryManager.lastChunk->size) {
        memoryChunk = (memory_chunk_t *) (
                (memoryManager.lastChunk && memoryManager.lastChunk->free ? (uint8_t *) memoryManager.lastChunk
                                                                          : (uint8_t *) mem0) + move - CONTROL);
        memoryChunk->next = NULL;
        memoryChunk->size = count;
        memoryChunk->free = 0;
        memoryChunk->unused = (intptr_t) custom_sbrk(0) - (intptr_t) memoryChunk - block;
        memoryChunk->fileline = fileline;
        memoryChunk->filename = (char *)filename;
        test(memoryChunk);

        if (memoryManager.lastChunk) {
            if (memoryManager.lastChunk->free) {
                memoryManager.lastChunk->size =
                        (intptr_t) memoryChunk - (intptr_t) memoryManager.lastChunk - HEADER_SIZE;
            } else if (move >= NEW_BEHIND) {
                newMemoryChunk = mem0;
                newMemoryChunk->prev = memoryManager.lastChunk;
                newMemoryChunk->next = memoryChunk;
                newMemoryChunk->size = (intptr_t) memoryChunk - (intptr_t) newMemoryChunk - HEADER_SIZE;
                newMemoryChunk->free = 1;
                newMemoryChunk->unused = 0;

                memoryManager.lastChunk->next = newMemoryChunk;
                memoryManager.lastChunk->fileline = fileline;
                memoryManager.lastChunk->filename = (char *)filename;
                test(memoryManager.lastChunk);
                memoryManager.lastChunk = newMemoryChunk;
            } else {
                memoryManager.lastChunk->unused = (intptr_t) memoryChunk - (intptr_t) memoryManager.lastChunk - GUARD -
                                                  memoryManager.lastChunk->size;
            }
            memoryChunk->prev = memoryManager.lastChunk;
            memoryManager.lastChunk->next = memoryChunk;
            memoryManager.lastChunk->fileline = fileline;
            memoryManager.lastChunk->filename = (char *)filename;
            test(memoryManager.lastChunk);
            memoryManager.lastChunk = memoryChunk;
        } else {
            newMemoryChunk = mem0;
            newMemoryChunk->prev = NULL;
            newMemoryChunk->next = memoryChunk;
            newMemoryChunk->size = (intptr_t) memoryChunk - (intptr_t) newMemoryChunk - HEADER_SIZE;
            newMemoryChunk->free = 1;
            newMemoryChunk->unused = 0;
            newMemoryChunk->fileline = fileline;
            newMemoryChunk->filename = (char *)filename;
            test(newMemoryChunk);
            memoryManager.firstChunk = newMemoryChunk;

            memoryManager.lastChunk = memoryChunk;
            memoryChunk->prev = newMemoryChunk;
        }
    } else {
        memoryChunk = (memory_chunk_t *) ((uint8_t *) memoryManager.lastChunk + move - CONTROL);
        if (move >= NEW_BEHIND) {
            memoryManager.lastChunk->size = (intptr_t) memoryChunk - (intptr_t) memoryManager.lastChunk - HEADER_SIZE;
            memoryManager.lastChunk->next = memoryChunk;
            memoryManager.lastChunk->fileline = fileline;
            memoryManager.lastChunk->filename = (char *)filename;
            test(memoryManager.lastChunk);
            memoryChunk->prev = memoryManager.lastChunk;
            memoryManager.lastChunk = memoryChunk;
        } else {
            memoryManager.lastChunk->prev->unused += (intptr_t) memoryChunk - (intptr_t) memoryManager.lastChunk;
            memoryManager.lastChunk->prev->next = memoryChunk;
            memoryManager.lastChunk->prev->fileline = fileline;
            memoryManager.lastChunk->prev->filename = (char *)filename;
            test(memoryManager.lastChunk->prev);
            memoryChunk->prev = memoryManager.lastChunk->prev;
            memoryManager.lastChunk = memoryChunk;
        }
        memoryChunk->next = NULL;
        memoryChunk->size = count;
        memoryChunk->free = 0;
        memoryChunk->unused = (intptr_t) custom_sbrk(0) - (intptr_t) memoryChunk - block;
    }
    heap_fences_set(memoryChunk);
    pthread_mutex_unlock(&memoryManager.mutex);
    return (uint8_t *) memoryChunk + CONTROL;
}

void *heap_calloc_aligned_debug(size_t number, size_t size, int fileline, const char *filename) {
    pthread_mutex_lock(&memoryManager.mutex);
    uint8_t *memoryChunk = heap_malloc_aligned_debug(number * size,fileline,filename);
    if (!memoryChunk) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    size_t fill = number * size;
    while (fill--)
        memoryChunk[fill] = 0;

    memory_chunk_t *secMemoryChunk = (memory_chunk_t *)(memoryChunk - FENCE_SIZE - HEADER_SIZE);
    secMemoryChunk->fileline = fileline;
    secMemoryChunk->filename = (char *)filename;
    test(secMemoryChunk);
    pthread_mutex_unlock(&memoryManager.mutex);
    return memoryChunk;
}

void *heap_realloc_aligned_debug(void *memblock, size_t size, int fileline, const char *filename) {
    pthread_mutex_lock(&memoryManager.mutex);
    if (heap_validate()) {
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }

    if (!memblock) {
        void *result = heap_malloc_aligned_debug(size,fileline,filename);
        pthread_mutex_unlock(&memoryManager.mutex);
        return result;
    }

    if (!size) {
        heap_free(memblock);
        pthread_mutex_unlock(&memoryManager.mutex);
        return NULL;
    }


    memory_chunk_t *memoryChunk = memoryManager.firstChunk;
    while (memoryChunk) {
        if ((uint8_t *) memoryChunk + CONTROL == (uint8_t *) memblock) {
            memoryChunk->fileline = fileline;
            memoryChunk->filename = (char *)filename;
            test(memoryChunk);
            if (((intptr_t) memblock & (intptr_t) (PAGE_SIZE - 1)) == 0) {
                if (memoryChunk->size == size) {
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
                if (memoryChunk->size > size) {
                    memoryChunk->unused = memoryChunk->size + memoryChunk->unused - size;
                    memoryChunk->size = size;
                    heap_fences_set(memoryChunk);
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
                if (memoryChunk->size + memoryChunk->unused >= size) {
                    memoryChunk->unused = memoryChunk->size + memoryChunk->unused - size;
                    memoryChunk->size = size;
                    heap_fences_set(memoryChunk);
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
                if (memoryChunk->next && memoryChunk->next->free &&
                    memoryChunk->size + memoryChunk->unused + HEADER_SIZE + memoryChunk->next->size >= size) {
                    memoryChunk->size += memoryChunk->unused;
                    memoryChunk->unused = 0;
                    if (memoryChunk->size + HEADER_SIZE + memoryChunk->next->size - size >= HEADER_SIZE) {
                        void *cnext = memoryChunk->next->next;
                        size_t csize = memoryChunk->next->size;
                        memory_chunk_t *newMemoryChunk = (memory_chunk_t *) ((intptr_t) memoryChunk + GUARD + size);
                        newMemoryChunk->size = memoryChunk->size + csize - size;
                        newMemoryChunk->prev = memoryChunk;
                        newMemoryChunk->next = cnext;
                        newMemoryChunk->free = 1;
                        newMemoryChunk->unused = 0;
                        newMemoryChunk->fileline = fileline;
                        newMemoryChunk->filename = (char *)filename;
                        test(newMemoryChunk);
                        if (newMemoryChunk->next) {
                            newMemoryChunk->next->prev = newMemoryChunk;
                            newMemoryChunk->next->fileline = fileline;
                            newMemoryChunk->next->filename = (char *)filename;
                            test(newMemoryChunk->next);
                        } else {
                            memoryManager.lastChunk = newMemoryChunk;
                        }
                        memoryChunk->next = newMemoryChunk;
                    } else {
                        size_t csize = memoryChunk->next->size;
                        memoryChunk->next = memoryChunk->next->next;
                        if (!memoryChunk->next) {
                            memoryManager.lastChunk = memoryChunk;
                        }
                        memoryChunk->unused += memoryChunk->size + HEADER_SIZE + csize - size;
                    }
                    memoryChunk->size = size;
                    heap_fences_set(memoryChunk);
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return memblock;
                }
            }
            if (memoryChunk == memoryManager.lastChunk) {
                void *newMemoryChunk = custom_sbrk(size - memoryChunk->size);
                if (newMemoryChunk == (void *) -1) {
                    pthread_mutex_unlock(&memoryManager.mutex);
                    return NULL;
                }
                memoryManager.size += size - memoryChunk->size;
                memoryChunk->size = size;
                heap_fences_set(memoryChunk);
                pthread_mutex_unlock(&memoryManager.mutex);
                return memblock;
            }

            void *newMemoryChunk = heap_malloc_aligned(size);
            if (!newMemoryChunk) {
                pthread_mutex_unlock(&memoryManager.mutex);
                return NULL;
            }

            mempcpy(newMemoryChunk, memblock, memoryChunk->size);
            heap_free(memblock);
            pthread_mutex_unlock(&memoryManager.mutex);
            return (uint8_t *) newMemoryChunk;
        }
        memoryChunk = memoryChunk->next;
    }
    pthread_mutex_unlock(&memoryManager.mutex);
    return NULL;
}

inline void test(memory_chunk_t *memoryChunk) {
    memoryChunk->test = memoryChunk->fileline + (intptr_t)memoryChunk->filename;
}
