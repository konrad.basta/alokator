//
// Created by root on 10/30/20.
//

#ifndef _ALLOCATOR_H
#define _ALLOCATOR_H

#include <stddef.h>
#include <inttypes.h>
#include <pthread.h>

enum pointer_type_t {
    pointer_null,               //Przekazany wskaźnik jest pusty – posiada wartość NULL
    pointer_heap_corrupted,     //Sterta jest uszkodzona
    pointer_control_block,      //Przekazany wskaźnik wskazuje na obszar struktur wewnętrznych/kontrolnych sterty
    pointer_inside_fences,      //Przekazany wskaźnik wskazuje na bajt, będący częścią dowolnego płotka dowolnego zajętego bloku
    pointer_inside_data_block,  //Przekazany wskaźnik wskazuje na środek któregoś z bloków, zaalokowanych przez użytkownika. Przez środek należy rozumieć adres bajta innego niż pierwszego danego bloku
    pointer_unallocated,        //Przekazany wskaźnik wskazuje na obszar wolny (niezaalokowany) lub poza stertę
    pointer_valid               //Przekazany wskaźnik jest poprawny. Wskazuje on na pierwszy bajt dowolnego bloku, przydzielonego użytkownikowi
};

typedef struct memory_manager_t {
    void * start;
    size_t size;
    struct memory_chunk_t * firstChunk;
    struct memory_chunk_t * lastChunk;
    pthread_mutex_t mutex;
    pthread_mutexattr_t mutexattr;
}memory_manager_t;

typedef struct memory_chunk_t {
    struct memory_chunk_t * prev;
    struct memory_chunk_t * next;
    size_t size;
    int free;
    int unused;
    size_t fileline;
    char *filename;
    size_t test;
}memory_chunk_t;

int heap_setup(void);
void heap_clean(void);

void* heap_malloc(size_t count);
void* heap_calloc(size_t number, size_t size);
void* heap_realloc(void* memblock, size_t count);
void  heap_free(void* memblock);

size_t   heap_get_largest_used_block_size(void);
enum pointer_type_t get_pointer_type(const void* const pointer);
int heap_validate(void);

void* heap_malloc_aligned(size_t count);
void* heap_calloc_aligned(size_t number, size_t size);
void* heap_realloc_aligned(void* memblock, size_t size);

void heap_fences_set(void* memblock);

void* heap_malloc_debug(size_t count, int fileline, const char* filename);
void* heap_calloc_debug(size_t number, size_t size, int fileline, const char* filename);
void* heap_realloc_debug(void* memblock, size_t size, int fileline, const char* filename);

void* heap_malloc_aligned_debug(size_t count, int fileline, const char* filename);
void* heap_calloc_aligned_debug(size_t number, size_t size, int fileline, const char* filename);
void* heap_realloc_aligned_debug(void* memblock, size_t size, int fileline, const char* filename);

void test(memory_chunk_t *memoryChunk);
#endif //_ALLOCATOR_H
